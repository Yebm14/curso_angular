import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {
  destinations: string[];  //Declaramos la variable destinations, un arreglo de tipo string

  constructor() {
    this.destinations = ['Medellin', 'Lima', 'Buenos Aires', 'Barcelona']; //Dentro del constructor se invoca la variable creada anteriormente
   }

  ngOnInit() {
  }

}
