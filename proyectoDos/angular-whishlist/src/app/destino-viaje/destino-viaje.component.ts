import { Component, OnInit, Input } from '@angular/core'; //Se colocan los artefactos o clases que va utilizar de angular o de otro paquete de código

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss']
})
export class DestinoViajeComponent implements OnInit {
  @Input() name : string;  //Nombre es pasado como parámetro (Input) en el <app-destino-viaje>

  constructor() { 
    
  }

  ngOnInit(): void {
  }

}
